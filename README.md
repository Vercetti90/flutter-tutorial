# Flutter Tutorial

# Inhaltsverzeichnis
- [Flutter Tutorial](#flutter-tutorial)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Quelle](#quelle)
- [Introduction](#introduction)
  - [Part 1: Foundations](#part-1-foundations)
    - [Dart](#dart)
      - [Syntax](#syntax)
      - [Loops](#loops)
      - [Branches](#branches)
      - [Object-oriented Programming](#object-oriented-programming)
      - [Packages](#packages)
      - [Inheritance, Mixins, Interfaces and Extensions](#inheritance-mixins-interfaces-and-extensions)
      - [Generators: Iterators and Streams](#generators-iterators-and-streams)
      - [Null-Safety](#null-safety)
    - [Flutter](#flutter)
      - [Examples](#examples)
        - [Debug Report](#debug-report)
        - [Navigator Two Pages](#navigator-two-pages)
      - [Flutter Packages](#flutter-packages)
  - [Part 2: Practice](#part-2-practice)
    - [Cloud Based Application](#cloud-based-application)
    - [Example](#example)
      - [Messenger App](#messenger-app)
    - [Desktop App](#desktop-app)


# Quelle

*Modern App Development with Dart and Flutter 2 - Dieter Meiller*
# Introduction

    Realize native apps cross-platform in only one programming language

**Native Apps**

These are apps programmed and optimized specifically for a platform

+ iOS: Objective-C, Swift
+ Android: Java, Kotlin

The graphical user interface is displayed with a render engine called **Skia**

## Part 1: Foundations

In this part, the syntax and the special features of the programming language **Dart** is discussed. Then the **Flutter Framework** is introduced with its philosophy, conventions and structure.

### Dart

Dart was designed for applcation developement and developed by Google since 2013. Originally for the web browser Chrome. Dart changed from an extension of JavaScript to a language (similarities to C# and Java). Offers dynamic and static typing.

Other languages:

+ **Groovy** (compatible with Java and generates code for the JVM)
+ **Go** (hardware-oriented language, can be used for system development, similar to C and C++)

Dart is available for three types of platforms:

1. Mobile Devices
2. The Console
3. The Web (AngularDart)

#### Syntax

```dart
helloWorld.dart

void main() {
    for(int i=0;i<5;i++){
    print('hello ${i+1}');
  }
}
```
+ Dart allows the use of **static** and **dynamic** typing (also **const** and **final**). There is also **type interference**. Types are permanently assigned to constants.
+ Before using variables and functions, their types must be specified. You can put **var** or **dynamic** in front of variables or functions (called **impicit typing, type reference or type derivation**)
+ Number types in Dart: **int, double** (Optimized memory management ensures that only as much memory as necessary is reserved, depending on the size of the number)
+ Both number types, int and double, inherit from type **num** (can store values of both number types)
+ All types get the default value zero (declared but not assigned a value). Dart-specific conditional assignment operator **??=for intialization** . This say that if the variable has the value zero, the expression on the right is assigned
+ In Dart all types are Obejects
+ Symbols are marked with a hash #
+ enum Direction {left, right, up, down}
+ Lambda expressions have the form (< Parameter >) => < Expression >
+ Function or typedef MyFunction
+ Two important types of collections in Dart: **maps and lists**
+ "~/" division operator for integer numbers ("%" = modulo operator)
+ **Protected Variables starts with an underscore "_" = _firstname, _lastname** (This means not visible outside their own package and protected from outside access)
+ **NO public, NO private (yet)**
+ x ? a : b = In Dart: a ?? b (If a is null, the expression become b, otherwise a)
+ Dart offers **Getter and Setter** with the keywords *get and set*. If you put them in front of method names, they can be addressed like object variabes
+ Immutable (Unveränderlich): final variables + const Constructor or use the package **meta** to annotate **@immutable**
+ Dependencies: Working directory must contain a so-called **pubspec.yaml** File that lists the dependencies (the dart source code belonding to the package should be located in a **lib/** Folder in the working directory)
+ **Pub is Darts own package management system**. With the console command *pub get*, external libraries are installed from the cloud to the local computer. 
+ keyword **yield**, like return, returns the following expression

#### Loops

+ Counting Loop
+ Do-While Loop
+ While Loop
+ For-In Loop
+ Foreach Method

#### Branches

+ If-Else
+ Switch case
+ with < Case-List > ::=
+ and < Case-Line > ::=

#### Object-oriented Programming

    Import of code, where you have to specify files and not classes or packages

All definitions of the imported files are then directly visible, not only classes but also other definitions like variables or functions

```dart
Person-1.dart

class Person {

}
-------------------------

main-1.dart

import 'Person-1.dart';
void main(){
  Person p = new Person()
}
```

**Class definition: class < Class name > { < Object variables > < Constructor > < Methods > }**

Packages do not exist in the form as in Java. You can store code files in folders in a structured way!

    Coding Style of Dart: You should specify the variable type only once (when declaring the object variables)

+ Constructor

```dart
Person (this._firstname, this._lastname);
```

Call a Constructor with varying numbers of parameters: Make parameters optional by putting them in Square Brackets 

```dart
Person (this._firstname, [this._lastname]);
```

+ Omit **new Keyword**

```dart
void main (){
  var p = Person ("John", "Smith");
}
```
#### Packages

    https://pub.dev/flutter/packages

#### Inheritance, Mixins, Interfaces and Extensions

**Mixins** allow the use of certain functionalities next to the inherited functionality!

With the keyword **with**, you can include abilities and properties of further classes into the existing class. Objects of this class are then also of the type of these classes. They must not have a constructor, since the super-constructor of the mixins cannot be called from the subclass.

```dart
class Student extends Person with Leaner{

}
```
    In Dart, you can specify all classes as interfaces 

There is the keyword **implemetens**, but no keyword **interface**. Interfaces are **abstract** classes in Dart

**Extension Methods**

Dart offers the possibility to extend existing classes with methods, getters and setters

```
extension [ < Name >] on < Type > { < Methods >}
```
#### Generators: Iterators and Streams

There are normal "**synchronous**" functions, **asynchronous** functions with async, and generator functions with sync* and async*

#### Null-Safety

    Variables must be declared explicity

Syntactically, this is express in Dart by adding a **question mark (?)** to the type

```dart
int? x2;

List<int>? x4;

List<int?> x5;
```
### Flutter

    Write once, Run Everywhere!

There are realizations for two design languages:

+ Cupertino
+ Material Design

Layouts are described as hierarchical dart data structures containing widgets and lists of widgets

    In Flutter everything is a widget ("Window")

Three types of widgets (inherit from the base class widget):

1. RenderObjectWidgets (layout properties)
2. StatelessWidgets (immubtable visible elements)
3. StatefulWidgets (variable state)

**Pages** are called "Routes" in Flutter. Routes are individual screens that can be navigated to and from. To create a Route, you must generate a route object and pass it to a **WidgetBuilder**. This is *lambda function* that return a widget.

#### Examples
##### Debug Report

```dart
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'List',
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.menu),title: Text('Bug Report'),
        ),
        body: ListView(
          children: <Widget>[
            for(int i=1;i<=30;i++)
            Card(
             elevation: 5.0,
             color: Colors.deepOrange,
             child: Column(
               mainAxisSize: MainAxisSize.min,
               children: <Widget>[
                 ListTile(
                   leading: Icon(Icons.bug_report,size: 50),
                   title: Text("Bug Nr. $i"),
                   subtitle: Text("Lorem Ipsum"),
                   onTap: () => print("Pressed: Nur.: $i"),
                 )
               ],
             ), 
            )
          ],
        )));
  }
}
```
##### Navigator Two Pages

```dart
import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(new RootApp());

class RootApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      home: PageOne(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class PageOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("One"),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Text("Page One"),
              MaterialButton(
                child: Text("Go to Page Two"),
                onPressed: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => PageTwo())),
              ),
            ],
          ),
        ),
        backgroundColor: Color(0xFFCCCCFF));
  }
}

class PageTwo extends StatelessWidget {
  static int _counter = 0;
  @override
  Widget build(BuildContext context) {
    _counter++;
    return Scaffold(
        appBar: AppBar(
          title: Text("Two"),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Text("Page Two Nr. $_counter"),
              MaterialButton(
                child: Text("Back to Page One"),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        ),
        backgroundColor: Color(Random().nextInt(0xFFFFFF) | 0xff << 24));
  }
}
```

#### Flutter Packages

A strength of Flutter is its modularity and expandability

    Central Repository: https://pub.dev/flutter

Flutter packages and plugins can be included in the **pubspec.yml** and downloaded via *flutter pub get*. For each component listed on the repository web page, there is a mini-homepage with good documentation.

If a Plugin is not null-safe:

    flutter run --no-sound-null-safety

## Part 2: Practice

In the second part of the book more complex projects are presented and explained: A cloud-based application, a drawing app for the desktop and game project that includes a web server.

### Cloud Based Application

    Modern web applications requires persistent storage

**Google Firebase** is a cloud service that integrates well with Flutter (https://console.firebase.google.com)

    Firebase is a so-called NoSQL database

After starting a new Project, you have to download the configuration file **google-services.json** and move it to **android/app** or **ios/app**. Classpath must be added:

```dart
buildscript{
  ...
  dependencies{
    ...
    classpath 'com.google.gms:google-services:4.3.4'
  }
}
```
There are two *build.gradle* files in the Android/iOS project. In both files different changes have to be made:

1. One in the folder android/ or ios/
2. One in the folder android/app/ or ios/app/

The Firebase database has three levels of hierarchy. In documents you can recusively create additional collections:

1. Collection
2. Documents
3. Fields

### Example
#### Messenger App

    ACID Principle (Atomicity, Consistency, Isolation, Durability)

A Firestore snapshot object, as available in Flutter, is a snapshot of the state of the database. This can change continuously so the snapshot may not be consisten with the true state of the database.

### Desktop App

